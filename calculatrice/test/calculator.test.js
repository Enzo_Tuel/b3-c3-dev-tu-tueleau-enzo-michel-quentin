const expect = require('chai').expect;
const Calculator = require('../src/calculator').Calculator;

describe('Testing calculator functions', function() {
    it('1. Test addition operation', function(done) {
        let value1 = 5;
        let value2 = 3;
        let t1 = new Calculator("");
        expect(t1.additionate(value1, value2)).to.equal(8)
        done();
    }); 
    it('2. Test substract operation', function(done) {
        let value1 = 5;
        let value2 = 3;
        let t2 = new Calculator("");
        expect(t2.subtract(value1, value2)).to.equal(2)
        done();
    });
    it('3. Test multiply operation', function(done) {
        let value1 = 5;
        let value2 = 3;
        let t3 = new Calculator("");
        expect(t3.multiply(value1, value2)).to.equal(15)
        done();
    });
    it('4. Test divide operation', function(done) {
        let value1 = 6;
        let value2 = 3;
        let t4 = new Calculator("");
        expect(t4.divide(value1, value2)).to.equal(2)
        done();
    });
    it('5. Test divide or multiply checking function', function(done) {
        let calcArray = [ "3.5", "*", "10", "+", "6", "/", "3", "-", "1" ];
        let t5 = new Calculator("3.5*(5+5)+6/Math.sqrt(Math.pow(3, 2))-Math.pow(1, 3)");
        expect(t5.checkIfMultiplyOrDivide(calcArray)).to.deep.equal(['35', '+', '2', '-', '1'])
        done();
    });
    it('6. Test additionate or subtract checking function', function(done) {
        let calcArray = [ 35, "+", 2, "-", 1 ]
        let t6 = new Calculator("3.5*(5+5)+6/Math.sqrt(Math.pow(3, 2))-Math.pow(1, 3)");
        expect(t6.checkIfAdditionateOrSubtract(calcArray)).to.deep.equal([36])
        done();
    });
    it('7. Test square root checking function', function(done) {
        let calcArray = [ "3.5", "*", "10", "+", "6", "/", "Math.sqrt(9)", "-", "1" ];
        let t7 = new Calculator("3.5*(5+5)+6/Math.sqrt(Math.pow(3, 2))-Math.pow(1, 3)");
        expect(t7.checkIfSquareRoot(calcArray)).to.deep.equal([ "3.5", "*", "10", "+", "6", "/", "3", "-", "1" ])
        done();
    });
    it('8. Test sqrt operation', function(done) {
        let value1 = "Math.sqrt(3*3)";
        let t8 = new Calculator("");
        expect(t8.applyCalc(value1)).to.equal(3)
        done();
    });
    it('9. Test power checking function', function(done) {
        let calcArray = [ "3.5", "*", "10", "+", "6", "/", "Math.sqrt(Math.pow(3, 2)", "-", "Math.pow(1, 3)" ];
        let t9 = new Calculator("3.5*(5+5)+6/Math.sqrt(Math.pow(3, 2))-Math.pow(1, 3)");
        expect(t9.checkIfPower(calcArray)).to.deep.equal([ "3.5", "*", "10", "+", "6", "/", "Math.sqrt(Math.pow(3, 2)", "-", "1" ])
        done();
    });
    it('10. Test square operation', function(done) {
        let value1 = "Math.pow(3+2, 2)";
        let t10 = new Calculator("");
        expect(t10.applyCalc(value1)).to.equal(25)
        done();
    });
    it('11. Test power operation', function(done) {
        let value1 = "Math.pow(3+2, 3)";
        let t11 = new Calculator("");
        expect(t11.applyCalc(value1)).to.equal(125)
        done();
    });
    it('12. Test parenthesis checking function', function(done) {
        let calcArray = [ "3.5", "*", "(5+5)", "+", "6", "/", "Math.sqrt(Math.pow(3, 2)", "-", "Math.pow(1, 3)" ];
        let t12 = new Calculator("3.5*(5+5)+6/Math.sqrt(Math.pow(3, 2))-Math.pow(1, 3)");
        expect(t12.checkIfParenthesis(calcArray)).to.deep.equal([ "3.5", "*", "10", "+", "6", "/", "Math.sqrt(Math.pow(3, 2)", "-", "Math.pow(1, 3)" ])
        done();
    });
    it('13. Test operation in parenthesis', function(done) {
        let value1 = "(3 + 10)";
        let t13 = new Calculator("");
        expect(t13.applyCalc(value1)).to.equal(13)
        done();
    });
});