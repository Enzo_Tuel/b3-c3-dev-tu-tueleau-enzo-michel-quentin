# b3-c3-dev-tu-TUELEAU-ENZO-MICHEL-QUENTIN



## Access to project

cd ./your_repo

git remote add origin https://gitlab.com/Enzo_Tuel/b3-c3-dev-tu-tueleau-enzo-michel-quentin.git 

## Install nodes modules

When you access to project, your are in the b3-c3-dev-tu-tueleau-enzo-michel-quentin folder but the node modules are in the calculator folder.

So you have to do: cd calculator

You can now install nodes modules with : npm install

## Launch tests 

After that you can launch tests with : npm test

## Open the calculator

If you want to manipulate the calculator, open the calculator.html file in a browser

## Calculator functions

sqrt(x) is the square Root function, you have to put the value that you want in parenthesis

pow(x,y) is the power function, you have to put the value you want to apply power instead of 'x' and to power value instead of 'y'

pow(x,2) is the square function, you have to put the value you want to apply power instead of 'x'

AC is for clean the calculator

## ANY PROBLEM ?

Contact us at : enzo.tueleau@epsi.fr or quentin.michel@epsi.fr