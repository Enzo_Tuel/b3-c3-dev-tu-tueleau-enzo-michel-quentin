class Calculator {

    constructor(calc) {
        this.calc = calc;
    }

    additionate(value1, value2) {
        return value1 + value2;
    }

    subtract(value1, value2) {
        return value1 - value2;
    }

    multiply(value1, value2) {
        return value1 * value2;
    }

    divide(value1, value2) {
        if (value2 !== 0) {
            return value1/value2;
        } else {
            throw 'Vous ne pouvez pas diviser par 0 !';
        }
    }
    applyCalc(calc) {
        calc = 'return ' + calc;
        let calculate = new Function(calc);
        let result = calculate();
        return result;
    }

    checkIfMultiplyOrDivide(calcArray) {
        let allValuesChecked = false;
        while (allValuesChecked === false) {
            let multiplyOrDivideIndex = 0;
            let valueChanged = false;
            for (const item of calcArray) {
                if (item === '*') {
                    let value1 = calcArray[multiplyOrDivideIndex - 1];
                    let value2 = calcArray[multiplyOrDivideIndex + 1];
                    let multipliedValue = this.multiply(value1, value2)
                    calcArray.splice(multiplyOrDivideIndex - 1, 3);
                    calcArray.splice(multiplyOrDivideIndex - 1, 0, multipliedValue.toString());
                    multiplyOrDivideIndex = multiplyOrDivideIndex;
                    valueChanged = true;
                } else if (item === '/') {
                    let value1 = calcArray[multiplyOrDivideIndex - 1];
                    let value2 = calcArray[multiplyOrDivideIndex + 1];
                    let multipliedValue = this.divide(value1, value2)
                    calcArray.splice(multiplyOrDivideIndex - 1, 3);
                    calcArray.splice(multiplyOrDivideIndex - 1, 0, multipliedValue.toString());
                    multiplyOrDivideIndex = multiplyOrDivideIndex;
                    valueChanged = true;
                } else if (calcArray.length === multiplyOrDivideIndex+1 && valueChanged === false) {
                    allValuesChecked = true;
                }
                multiplyOrDivideIndex++;
            }
        }
        return calcArray;
    }

    checkIfAdditionateOrSubtract(calcArray) {
        let allValuesChecked = false;
        while (allValuesChecked === false) {
            let additionateOrSubtractIndex = 0;
            let valueChanged = false;
            for (const item of calcArray) {
                if (item === '+') {
                    let value1 = calcArray[additionateOrSubtractIndex - 1];
                    let value2 = calcArray[additionateOrSubtractIndex + 1];
                    let additionatedValue = this.additionate(Number(value1), Number(value2))
                    calcArray.splice(additionateOrSubtractIndex - 1, 3);
                    calcArray.splice(additionateOrSubtractIndex - 1, 0, additionatedValue);
                    additionateOrSubtractIndex = additionateOrSubtractIndex;
                    valueChanged = true;
                } else if (item === '-') {
                    let value1 = calcArray[additionateOrSubtractIndex - 1];
                    let value2 = calcArray[additionateOrSubtractIndex + 1];
                    let subtractedValue = this.subtract(Number(value1), Number(value2))
                    calcArray.splice(additionateOrSubtractIndex - 1, 3);
                    calcArray.splice(additionateOrSubtractIndex - 1, 0, subtractedValue);
                    additionateOrSubtractIndex = additionateOrSubtractIndex;
                    valueChanged = true;
                } else if (calcArray.length === additionateOrSubtractIndex+1 && valueChanged === false) {
                    allValuesChecked = true;
                }
                additionateOrSubtractIndex++;
            }
        }
        return calcArray;
    }

    checkIfPower(calcArray) {
        let allValuesChecked = false;
        while (allValuesChecked === false) {
            let powerIndex = 0;
            let valueChanged = false;
            for(const item of calcArray) {
                if (item.toString().indexOf('p') === 5) {
                    let itemCalculated = this.applyCalc(item);
                    calcArray.splice(powerIndex, 1);
                    calcArray.splice(powerIndex, 0, itemCalculated.toString());
                    valueChanged = true;
                } else if (calcArray.length === powerIndex+1 && valueChanged === false) {
                    allValuesChecked = true;
                }
                powerIndex++;
            }
        }
        return calcArray;
    }

    checkIfSquareRoot(calcArray) {
        let allValuesChecked = false;
        while (allValuesChecked === false) {
            let sqrtIndex = 0;
            let valueChanged = false;
            for(const item of calcArray) {
                if (item.toString().indexOf('s') === 5) {
                    let itemCalculated = this.applyCalc(item);
                    calcArray.splice(sqrtIndex, 1);
                    calcArray.splice(sqrtIndex, 0, itemCalculated.toString());
                    valueChanged = true;
                } else if (calcArray.length === sqrtIndex+1 && valueChanged === false) {
                    allValuesChecked = true;
                }
                sqrtIndex++;
            }
        }
        return calcArray;
    }

    checkIfParenthesis(calcArray) {
        let allValuesChecked = false;
        while (allValuesChecked === false) {
            let parenthesisIndex = 0;
            let valueChanged = false;
            for(const item of calcArray) {
                if (item.toString().indexOf('(') === 0) {
                    let itemCalculated = this.applyCalc(item);
                    calcArray.splice(parenthesisIndex, 1);
                    calcArray.splice(parenthesisIndex, 0, itemCalculated.toString());
                    valueChanged = true;
                } else if (calcArray.length === parenthesisIndex+1 && valueChanged === false) {
                    allValuesChecked = true;
                }
                parenthesisIndex++;
            }
        }
        return calcArray;
    }

}

if (typeof module !== 'undefined') {
    module.exports = {
        Calculator : Calculator
    }; // On node.js, use exports
} else if (window) {
    window.Calculator = Calculator;
} else {
    console.error('Unknown environment');
}