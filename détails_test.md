# b3-c3-dev-tu-TUELEAU-ENZO-MICHEL-QUENTIN

## FICHIER EXPLICATIF DES TESTS UTILISÉS

1. Test addition operation :
    Fonctionnement : 
        - créer 2 variables, value1 et value2, puis les initialisent
        - ensuite créer une variable t1 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'additionate' et compare le résultat avec celui voulu
    
    Fonction 'additionate' : 
        - focntion d'addition entre la value1 et la value2
    
    Objectif : 
        - on test la fonction 'additionate' à savoir si il fait bien une addition des deux valeurs donnés et renvoi le bon résultat
        - ici tester : 5+3 = 8

2. Test substract operation
    Fonctionnement : 
        - créer 2 variables, value1 et value2, puis les initialisent
        - ensuite créer une variable t2 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'subtract' et compare le résultat avec celui voulu
    
    Fonction 'subtract' : 
        - focntion de soustraction entre la value1 et la value2
    
    Objectif : 
        - on test la fonction 'subtract' à savoir si il fait bien une soustraction des deux valeurs donnés et renvoi le bon résultat
        - ici tester : 5-3 = 2

3. Test multiply operation
    Fonctionnement : 
        - créer 2 variables, value1 et value2, puis les initialisent
        - ensuite créer une variable t3 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'multiply' et compare le résultat avec celui voulu
    
    Fonction 'multiply' : 
        - focntion de multiplication entre la value1 et la value2
    
    Objectif : 
        - on test la fonction 'multiply' à savoir si il fait bien une multipication des deux valeurs donnés et renvoi le bon résultat
        - ici tester : 5*3 = 15

4. Test divide operation
    Fonctionnement : 
        - créer 2 variables, value1 et value2, puis les initialisent
        - ensuite créer une variable t4 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'divide' et compare le résultat avec celui voulu
    
    Fonction 'divide' : 
        - fonction de division entre la value1 et la value2
    
    Objectif : 
        - on test la fonction 'divide' à savoir si il fait bien une divisiob des deux valeurs donnés et renvoi le bon résultat
        - ici tester : 6/3 = 2

5. Test divide or multiply checking function
    Fonctionnement :
        - créer une variable 'calcArray' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t5 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'checkIfMultiplyOrDivide' et compare le résultat avec celui voulu

    Fonction checkIfMultiplyOrDivide : 
        - fonction qui vérifie chaque case du tableau donné et regarde quand il y a une '*' ou un '/' il effectue le calcul lié, soit une multiplication soit une division

    Objectif :
        - on test la fonctionnalité de séparation des calculs par la calculatrice, ici faire d'abord les multiplications et/ou les divisions
    
6. Test additionate or subtract checking function
    Fonctionnement :
        - créer une variable 'calcArray' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t6 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'checkIfAdditionateOrSubtract' et compare le résultat avec celui voulu

    Fonction checkIfAdditionateOrSubtract : 
        - fonction qui vérifie chaque case du tableau donné et regarde quand il y a une '+' ou un '-' il effectue le calcul lié, soit une addition soit une soustraction

    Objectif :
        - on test la fonctionnalité de séparation des calculs par la calculatrice, ici faire les additions et/ou soustraction restant dans le calcul

7. Test square root checking function
    Fonctionnement :
        - créer une variable 'calcArray' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t7 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'checkIfSquareRoot' et compare le résultat avec celui voulu

    Fonction checkIfSquareRoot : 
        - fonction qui vérifie chaque case du tableau donné et regarde quand il y a une 's' il effectue le calcul lié

    Objectif :
        - on test la fonctionnalité de séparation des calculs par la calculatrice, ici faire les racine carré dans le calcul

8. Test sqrt operation
    Fonctionnement :
        - créer une variable 'value1' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t8 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'applyCalc' et compare le résultat avec celui voulu

    Fonction 'applyCalc' : 
        - fonction qui transforme la chaine envoyé en fonction et résous le calcul
    
    Objectif : 
        - on test la fonction 'applyCalc' à savoir si il fait bien le calcul, ici si il réalise bien la racine carré

9. Test power checking function
    Fonctionnement :
        - créer une variable 'calcArray' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t8 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'checkIfPower' et compare le résultat avec celui voulu

    Fonction 'checkIfPower' : 
        - fonction qui vérifie chaque case du tableau donné et regarde quand il y a une 'p' il effectue le calcul lié
    
    Objectif : 
        - on test la fonctionnalité de séparation des calculs par la calculatrice, ici faire les calculs de puissance

10. Test square operation
    Fonctionnement :
        - créer une variable 'value1' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t10 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'applyCalc' et compare le résultat avec celui voulu

    Fonction 'applyCalc' : 
        - fonction qui transforme la chaine envoyé en fonction et résous le calcul
    
    Objectif : 
        - on test la fonction 'applyCalc' à savoir si il fait bien le calcul, ici si il réalise bien le carré

11. Test power operation
    Fonctionnement :
        - créer une variable 'value1' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t11 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'applyCalc' et compare le résultat avec celui voulu

    Fonction 'applyCalc' : 
        - fonction qui transforme la chaine envoyé en fonction et résous le calcul
    
    Objectif : 
        - on test la fonction 'applyCalc' à savoir si il fait bien le calcul, ici si il réalise bien la puissance, ici puissance 3

12. Test parenthesis checking function
    Fonctionnement :
        - créer une variable 'calcArray' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t12 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'checkIfParenthesis' et compare le résultat avec celui voulu

    Fonction 'checkIfParenthesis' : 
        - fonction qui vérifie chaque case du tableau donné et regarde quand il y a une '(' il effectue le calcul lié
    
    Objectif : 
        - on test la fonctionnalité de séparation des calculs par la calculatrice, ici faire les calculs de puissance

13. Test operation in parenthesis
    Fonctionnement :
        - créer une variable 'value1' et donne en valeur un tableau de chaine contenant le calcul donné par la calculatrice
        - ensuite créer une variable t13 et l'instancie en objet de type Calculator
        - fini avec l'appel de la fonction 'applyCalc' et compare le résultat avec celui voulu

    Fonction 'applyCalc' : 
        - fonction qui transforme la chaine envoyé en fonction et résous le calcul
    
    Objectif : 
        - on test la fonction 'applyCalc' à savoir si il fait bien le calcul, ici si il réalise bien le calcul entre parenthèse, ici une addition

